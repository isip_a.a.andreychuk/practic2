import sys


def matrix():
    try:
        number_columns = int(input("Введите количество столбцов "))
        number_lines = int(input("Введите количество строк "))
        first = int(input("Введите значение с которого начинается матрица "))
        step = int(input("Введите шаг, на который должны увеличиваться значения "))
        matr = [[0 for x in range(number_columns)] for y in range(number_lines)]
        for x in range(number_lines):
            for y in range(number_columns):
                matr[x][y] = first
                first += step
        for i in matr:
            print(*i)
        print("\n")
        print("0 - Вернуться в меню")
        print("1 - Продолжить вывод матриц")
        try:
            choiz = int(input("Ваш выбор: "))
            match choiz:
                case 0:
                    main()
                case 1:
                    calcul()
        except ValueError as er:
            print(er)
            print("Необходимо вводить только цифры 0 или 1")
    except ValueError as er:
        print(er)
        print("Необходимо вводить только цифры")
        matrix()


def read_str():
    try:
        str = input("Введите строку: ")
        space = 0
        comma = 0
        for char in str:
            if char == " ":
                space += 1
            elif char == ",":
                comma += 1
        print('Количество пробелов - {0}, количество запятых - {1}, количество символов - {2}'.format(space, comma,
                                                                                                      len(str)))
        print("\n")
        print("0 - Вернуться в меню")
        print("1 - Продолжить работу со строками")
        try:
            ch = int(input("Ваш выбор: "))
            match ch:
                case 0:
                    main()
                case 1:
                    calcul()
        except ValueError as er:
            print(er)
            print("Необходимо вводить только цифры 0 или 1")
    except ValueError as er:
        print(er)
        return


def calcul():
    try:
        print(eval(input("Введите выражение, которое необходимо посчитать - ")))
        print("\n")
        print("0 - Вернуться в меню")
        print("1 - Продолжить подсчёт")

        try:
            cho = int(input("Ваш выбор: "))
            match cho:
                case 0:
                    main()
                case 1:
                    calcul()
        except ValueError as er:
            print(er)
            print("Необходимо вводить только цифры 0 или 1")
    except:
        print("Необходимо вводить только цифры и математические, кроме =")
        calcul()


def main():
    print("1 - Калькулятор \n"
          "2 - Действия со строкой \n"
          "3 - Матрица \n"
          "0 - Выход")

    try:
        choize = int(input("Введите необходимое действие: "))
        match choize:
            case 0:
                sys.exit()
            case 1:
                calcul()
            case 2:
                read_str()
            case 3:
                matrix()
    except KeyboardInterrupt:
        sys.exit()
    except ValueError as er:
        print(er)
        print("Необходимо вводить только цифры")


while True:
    main()
