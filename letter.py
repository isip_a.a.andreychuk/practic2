def read_str():
    try:
        str = input("Введите строку: ")
        space = 0
        comma = 0
        for char in str:
            if char == " ":
                space += 1
            elif char == ",":
                comma += 1
        print('Количество пробелов - {0}, количество запятых - {1}, количество символов - {2}'.format(space, comma,
                                                                                                      len(str)))
        print("\n")
        print("0 - Вернуться в меню")
        print("1 - Продолжить работу со строками")
        try:
            ch = int(input("Ваш выбор: "))
            match ch:
                case 0:
                    main()
                case 1:
                    calcul()
        except ValueError as er:
            print(er)
            print("Необходимо вводить только цифры 0 или 1")
    except ValueError as er:
        print(er)
        return
