    try:
        number_columns = int(input("Введите количество столбцов "))
        number_lines = int(input("Введите количество строк "))
        first = int(input("Введите значение с которого начинается матрица "))
        step = int(input("Введите шаг, на который должны увеличиваться значения "))
        matr = [[0 for x in range(number_columns)] for y in range(number_lines)]
        for x in range(number_lines):
            for y in range(number_columns):
                matr[x][y] = first
                first += step
        for i in matr:
            print(*i)
        print("\n")
        print("0 - Вернуться в меню")
        print("1 - Продолжить вывод матриц")
        try:
            choiz = int(input("Ваш выбор: "))
            match choiz:
                case 0:
                    main()
                case 1:
                    calcul()
        except ValueError as er:
            print(er)
            print("Необходимо вводить только цифры 0 или 1")
    except ValueError as er:
        print(er)
        print("Необходимо вводить только цифры")
        matrix()
