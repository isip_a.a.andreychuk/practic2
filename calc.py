def calcul():
    try:
        print(eval(input("Введите выражение, которое необходимо посчитать - ")))
        print("\n")
        print("0 - Вернуться в меню")
        print("1 - Продолжить подсчёт")

        try:
            cho = int(input("Ваш выбор: "))
            match cho:
                case 0:
                    main()
                case 1:
                    calcul()
        except ValueError as er:
            print(er)
            print("Необходимо вводить только цифры 0 или 1")
    except:
        print("Необходимо вводить только цифры и математические, кроме =")
        calcul()